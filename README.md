#WiFi Turtlebot Project#

Implementation of inexpensive quadruped robot using 3D print model from
[thingiverse](http://tinyurl.com/m4qh3ek) and Raspberry Pi. Differently this project is to use Linux as the runtime platform. The robot is intended to be remotely controlled via WiFi by any mobile devices or computers with a web browser.

![1791084488-2526438444-turtlebot.png](https://bitbucket.org/repo/jXgxLG/images/3588269963-1791084488-2526438444-turtlebot.png)

The program is written to perform very basic gaits. You are welcome to improve the gaits and adding sensors to make it becoming more intelligent.

##Project Blog 
* [http://wifiquadrupedrobot.blogspot.com/](https://wifiquadrupedrobot.blogspot.com/)

## Demo of implementations ##
Video - click [here](http://youtu.be/iae71xAs5AU) and [here](http://youtu.be/e13zMQE16ZQ)

## Main components include ##

* Raspberry Pi Model B/B+
* [3D model](http://tinyurl.com/m4qh3ek) of the body
* 8 x 9g Servos
* 5V2A Power bank
* An USB WiFi Dongle, e.g Ralink RT5370 USB Wireless Adapter

## Building Turtlebot & Installation of Turtlebot Program
See Wiki