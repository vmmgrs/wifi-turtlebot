/* 
 * File:   ServoBlaster.h
 * Author: user
 *
 * Created on July 29, 2014, 2:15 PM
 */

#ifndef SERVOBLASTER_H
#define	SERVOBLASTER_H
#ifdef	__cplusplus
extern "C" {
#endif
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <syslog.h>

#ifdef	__cplusplus
}
#endif
class ServoBlaster {
public:
    ServoBlaster();
    ServoBlaster(const ServoBlaster& orig);
    virtual ~ServoBlaster();
    void runServo(int degrees[], int total_servos, int delay);
private:
    int fd;

};

#endif	/* SERVOBLASTER_H */

