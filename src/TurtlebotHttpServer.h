/* 
 * File:   TurtlebotHttpServer.h
 * Author: user
 *
 * Created on July 6, 2014, 2:37 PM
 */

#ifndef TURTLEBOTHTTPSERVER_H
#define	TURTLEBOTHTTPSERVER_H

#include <cstdlib>
#include <cstdio>
#include "TurtlebotKine.h"
#ifdef	__cplusplus
extern "C" {
#endif
#include <unistd.h>
#include <syslog.h>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <libwebsockets.h>

#ifdef	__cplusplus
}
#endif

class TurtlebotHttpServer : public TurtlebotKine {
public:
    TurtlebotHttpServer();
    void start_turtlebot_server();
    void stop_turtlebot_server();
    virtual ~TurtlebotHttpServer();
    friend void *turtlebot_server_thread_kine(void *parm);
    friend void *turtlebot_server_thread_http(void *parm);
    friend void *turtlebot_server_thread_initial_action(void *parm);
    friend int callback_http(struct libwebsocket_context *context,
            struct libwebsocket *wsi,
            enum libwebsocket_callback_reasons reason, void *user,
            void *in, size_t len);
private:
    int action;
    void *loop;
};

#endif	/* TURTLEBOTHTTPSERVER_H */

