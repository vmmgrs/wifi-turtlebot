/* 
 * File:   TurtlebotKine.h
 * Author: user
 *
 * Created on February 27, 2014, 12:20 PM
 */

#ifndef TURTLEBOT_KINE_H
#define	TURTLEBOT_KINE_H

#include "ServoBlaster.h"

#define TURTLEBOT_GO_FORWARD  1
#define TURTLEBOT_GO_BACKWARD 2
#define TURTLEBOT_TURN_LEFT   3
#define TURTLEBOT_TURN_RIGHT  4
#define TURTLEBOT_UP_DOWN     5
#define TURTLEBOT_STAND_BY    6
#define TURTLEBOT_DANCE       7
#define TURTLEBOT_STOP        8

class TurtlebotKine : public ServoBlaster {
public:
    TurtlebotKine();
    void setSpeed(int value);
    void _1_goForward();
    void _2_goBackward();
    void _3_turnLeft();
    void _4_turnRight();
    void _6_stand();
    void _5_upAndDown();
    void _7_dance();
    void legsUp();
    void legsDown();
private:
    int *ptr, delay;

};

#endif	/* TURTLEBOT_KINE_H */

