/* 
 * File:   TurtlebotHttpServer.cpp
 * Author: user
 * 
 * Created on July 6, 2014, 2:37 PM
 */

#include "TurtlebotHttpServer.h"
#include <cstring>

extern void *turtlebot_server_thread_http(void *parm);
extern void *turtlebot_server_thread_kine(void *parm);
extern void *turtlebot_server_thread_initial_action(void *parm);
extern int callback_http(
        struct libwebsocket_context * context,
        struct libwebsocket *wsi,
        enum libwebsocket_callback_reasons reason,
        void *user,
        void *in,
        size_t len);

static struct libwebsocket_protocols protocols[] = {
    {
        "http-only",
        callback_http,
        0
    },
    {
        NULL, NULL, 0
    }
};

TurtlebotHttpServer::TurtlebotHttpServer() {
    action = TURTLEBOT_STAND_BY;
}

void TurtlebotHttpServer::start_turtlebot_server() {
    pthread_t thread1, thread2, thread3;
    loop = this;
    pthread_create(&thread1, NULL, turtlebot_server_thread_kine, (void*) this);
    pthread_create(&thread2, NULL, turtlebot_server_thread_http, (void*) this);
    pthread_create(&thread3, NULL, turtlebot_server_thread_initial_action, (void*) this);
    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);
    pthread_join(thread3, NULL);

}

void TurtlebotHttpServer::stop_turtlebot_server() {
    loop = NULL;
}

TurtlebotHttpServer::~TurtlebotHttpServer() {
}

void *turtlebot_server_thread_initial_action(void *parm) {
    TurtlebotHttpServer *turtleKine = (TurtlebotHttpServer*) parm;
    syslog(LOG_NOTICE, "turtlebot_server_thread_initial_action():starting...\n");
    sleep(3);
    turtleKine->action = TURTLEBOT_UP_DOWN;
    sleep(3);
    turtleKine->action = TURTLEBOT_STAND_BY;

}
void *turtlebot_server_thread_http(void *parm) {
    TurtlebotHttpServer *turtleKine = (TurtlebotHttpServer*) parm;
    // create connection struct
    struct libwebsocket_context *context;
    struct lws_context_creation_info info;
    info.port = 9000;
    info.iface = NULL;
    info.protocols = protocols;
    info.extensions = NULL;
    info.ssl_cert_filepath = NULL;
    info.ssl_private_key_filepath = NULL;
    info.options = 0; // no special options
    info.uid = -1; //getuid();
    info.gid = -1; //getgid();
    info.user = turtleKine;
    // create libwebsocket context representing this server
    context = libwebsocket_create_context(&info);
    if (context == NULL) {
        syslog(LOG_NOTICE, "turtlebot_server_thread_http(): libwebsocket init failed\n");
        return NULL;
    }
    syslog(LOG_NOTICE, "turtlebot_server_thread_http():running websocket server...\n");
    while (turtleKine->loop) {
        libwebsocket_service(context, 10);
    }
    syslog(LOG_NOTICE, "turtlebot_server_thread_http(): websocket server ended.\n");
    libwebsocket_context_destroy(context);
    return NULL;

}

void *turtlebot_server_thread_kine(void *parm) {
    TurtlebotHttpServer *turtleKine = (TurtlebotHttpServer*) parm;
    while (turtleKine->loop) {
        syslog(LOG_NOTICE, "turtlebot_server_thread_kine(): action is %d\n", turtleKine->action);
        switch (turtleKine->action) {
            case TURTLEBOT_GO_FORWARD:
                turtleKine->_1_goForward();
                //syslog(LOG_NOTICE, "turtlebot_server_thread_kine(): goForward\n");
                break;
            case TURTLEBOT_GO_BACKWARD:
                turtleKine->_2_goBackward();
                //syslog(LOG_NOTICE, "turtlebot_server_thread_kine(): goBackward\n");
                break;
            case TURTLEBOT_TURN_LEFT:
                turtleKine->_3_turnLeft();
                //syslog(LOG_NOTICE, "turtlebot_server_thread_kine(): turnLeft\n");
                break;
            case TURTLEBOT_TURN_RIGHT:
                turtleKine->_4_turnRight();
                //syslog(LOG_NOTICE, "turtlebot_server_thread_kine(): turnRight\n");
                break;
            case TURTLEBOT_UP_DOWN:
                turtleKine->_5_upAndDown();
                //syslog(LOG_NOTICE, "turtlebot_server_thread_kine(): turnRight\n");
                break;
            case TURTLEBOT_STAND_BY:
                turtleKine->_6_stand();
                turtleKine->action = TURTLEBOT_STOP;
                break;
            case TURTLEBOT_STOP:
                sleep(1);
                break;
            default:
                turtleKine->action = TURTLEBOT_STAND_BY;
                break;
        }
    }
    turtleKine->_6_stand();
    syslog(LOG_NOTICE, "turtlebot_server_thread_kine(): ended\n");

    return NULL;
}

int callback_http(struct libwebsocket_context *context,
        struct libwebsocket *wsi,
        enum libwebsocket_callback_reasons reason, void *user,
        void *in, size_t len) {
    int rc;

    TurtlebotHttpServer *turtleKine = (TurtlebotHttpServer*) libwebsocket_context_user(context);
    syslog(LOG_NOTICE, "callback_http():action is %d, 0x%p\n", turtleKine->action, turtleKine);
    switch (reason) {
        case LWS_CALLBACK_HTTP:
        {
            char *requested_uri = (char *) in;
            //printf("requested URI: %s\n", requested_uri);
            if (strcmp(requested_uri, "/favicon.ico") == 0) {

                rc = libwebsockets_serve_http_file(context, wsi, "favicon.ico", "image/x-icon");
            } else if (strcmp(requested_uri, "/forward") == 0) {
                turtleKine->action = TURTLEBOT_GO_FORWARD;
            } else if (strcmp(requested_uri, "/left") == 0) {
                turtleKine->action = TURTLEBOT_TURN_LEFT;
            } else if (strcmp(requested_uri, "/stand") == 0) {
                turtleKine->action = TURTLEBOT_STAND_BY;
            } else if (strcmp(requested_uri, "/right") == 0) {
                turtleKine->action = TURTLEBOT_TURN_RIGHT;
            } else if (strcmp(requested_uri, "/backward") == 0) {
                turtleKine->action = TURTLEBOT_GO_BACKWARD;
            } else if (strcmp(requested_uri, "/pushup") == 0) {
                turtleKine->action = TURTLEBOT_UP_DOWN;
            }
            char resource_path[1024] = {0};
            if (getcwd(resource_path, sizeof (resource_path)) != NULL) {
                strcat(resource_path, "/index.html");
                //syslog(LOG_NOTICE, "callback_http():resource path: %s\n", resource_path);
                libwebsockets_serve_http_file(context, wsi, resource_path, "text/html");
            } else {
                char error[20] = "Error!";
                libwebsocket_write(wsi, (unsigned char *) error, strlen(error), LWS_WRITE_HTTP);
            }
            return -1; // -1 close http connection
        }
        default:
            //printf("unhandled callback\n");
            break;
    }

    return 0;
}