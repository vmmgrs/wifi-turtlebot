/* 
 * File:   TurtlebotKine.cpp
 * Author: user
 * 
 * Created on February 27, 2014, 12:20 PM
 */

#include "TurtlebotKine.h"


#define s1_right_front_thigh_index 0
#define s2_right_front_leg_index 1
#define s3_right_back_thigh_index 2
#define s4_right_back_leg_index 3
#define s5_left_back_thigh_index 4
#define s6_left_back_leg_index 5
#define s7_left_front_thigh_index 6
#define s8_left_front_leg_index 7
#define total_joins s8_left_front_leg_index+1

#define s2_right_front_leg_up 50
#define s4_right_back_leg_dn 180  
#define s6_left_back_leg_up 50
#define s8_left_front_leg_dn 180

#define s2_right_front_leg_dn  0 
#define s4_right_back_leg_up  100
#define s6_left_back_leg_dn  0 
#define s8_left_front_leg_up  100  

#define s2_right_front_leg_center  36 
#define s4_right_back_leg_center  117 
#define s6_left_back_leg_center  36
#define s8_left_front_leg_center  117 

#define s1_right_front_thigh_open 54
#define s3_right_back_thigh_close 117
#define s5_left_back_thigh_close 162
#define s7_left_front_thigh_open 25

#define s1_right_front_thigh_close 150
#define s3_right_back_thigh_open 25
#define s5_left_back_thigh_open 54
#define s7_left_front_thigh_close 135

#define s1_right_front_thigh_center 117
#define s3_right_back_thigh_center 54
#define s5_left_back_thigh_center 117
#define s7_left_front_thigh_center 54

TurtlebotKine::TurtlebotKine() {
    setSpeed(500);
}

void TurtlebotKine::setSpeed(int value) {
    if (value < 500) value=500;
    delay = value;
}

void TurtlebotKine::legsDown() {
    int all_dn[total_joins];
    ptr = all_dn;
    ptr[s1_right_front_thigh_index] = s1_right_front_thigh_center;
    ptr[s2_right_front_leg_index] = s2_right_front_leg_dn;
    ptr[s3_right_back_thigh_index] = s3_right_back_thigh_center;
    ptr[s4_right_back_leg_index] = s4_right_back_leg_dn;
    ptr[s5_left_back_thigh_index] = s5_left_back_thigh_center;
    ptr[s6_left_back_leg_index] = s6_left_back_leg_dn;
    ptr[s7_left_front_thigh_index] = s7_left_front_thigh_center;
    ptr[s8_left_front_leg_index] = s8_left_front_leg_dn;
    runServo(ptr, total_joins, delay);
}

void TurtlebotKine::legsUp() {
    int all_up[total_joins];
    ptr = all_up;
    ptr[s1_right_front_thigh_index] = s1_right_front_thigh_center;
    ptr[s2_right_front_leg_index] = s2_right_front_leg_up;
    ptr[s3_right_back_thigh_index] = s3_right_back_thigh_center;
    ptr[s4_right_back_leg_index] = s4_right_back_leg_up;
    ptr[s5_left_back_thigh_index] = s5_left_back_thigh_center;
    ptr[s6_left_back_leg_index] = s6_left_back_leg_up;
    ptr[s7_left_front_thigh_index] = s7_left_front_thigh_center;
    ptr[s8_left_front_leg_index] = s8_left_front_leg_up;
    runServo(ptr, total_joins, delay);

}

void TurtlebotKine::_5_upAndDown() {
    legsUp();
    legsDown();
}

void TurtlebotKine::_6_stand() {
    int stand[total_joins];
    ptr = stand;
    ptr[s1_right_front_thigh_index] = s1_right_front_thigh_center;
    ptr[s2_right_front_leg_index] = s2_right_front_leg_center;
    ptr[s3_right_back_thigh_index] = s3_right_back_thigh_center;
    ptr[s4_right_back_leg_index] = s4_right_back_leg_center;
    ptr[s5_left_back_thigh_index] = s5_left_back_thigh_center;
    ptr[s6_left_back_leg_index] = s6_left_back_leg_center;
    ptr[s7_left_front_thigh_index] = s7_left_front_thigh_center;
    ptr[s8_left_front_leg_index] = s8_left_front_leg_center;
    runServo(ptr, total_joins, delay);
}

void TurtlebotKine::_1_goForward() {
    int forward_a[total_joins]; //left thighs open &legs up/dn , right thighs close & legs dn/up
    ptr = forward_a;
    ptr[s1_right_front_thigh_index] = s1_right_front_thigh_open;
    ptr[s2_right_front_leg_index] = s2_right_front_leg_up;
    ptr[s3_right_back_thigh_index] = s3_right_back_thigh_close;
    ptr[s4_right_back_leg_index] = s4_right_back_leg_dn;
    ptr[s5_left_back_thigh_index] = s5_left_back_thigh_close;
    ptr[s6_left_back_leg_index] = s6_left_back_leg_up;
    ptr[s7_left_front_thigh_index] = s7_left_front_thigh_open;
    ptr[s8_left_front_leg_index] = s8_left_front_leg_dn;
    runServo(ptr, total_joins, delay);

    int forward_b[total_joins]; // left thighs close & legs dn/up, right thighs open & legs up/dn
    ptr = forward_b;
    ptr[s1_right_front_thigh_index] = s1_right_front_thigh_close;
    ptr[s2_right_front_leg_index] = s2_right_front_leg_dn;
    ptr[s3_right_back_thigh_index] = s3_right_back_thigh_open;
    ptr[s4_right_back_leg_index] = s4_right_back_leg_up;
    ptr[s5_left_back_thigh_index] = s5_left_back_thigh_open;
    ptr[s6_left_back_leg_index] = s6_left_back_leg_dn;
    ptr[s7_left_front_thigh_index] = s7_left_front_thigh_close;
    ptr[s8_left_front_leg_index] = s8_left_front_leg_up;
    runServo(ptr, total_joins, delay);
}

void TurtlebotKine::_2_goBackward() {
 
}

void TurtlebotKine::_3_turnLeft() {
    int turn_left_a[total_joins];
    ptr = turn_left_a;
    ptr[s1_right_front_thigh_index] = s1_right_front_thigh_close;
    ptr[s2_right_front_leg_index] = s2_right_front_leg_center;
    ptr[s3_right_back_thigh_index] = s3_right_back_thigh_center;
    ptr[s4_right_back_leg_index] = s4_right_back_leg_dn;
    ptr[s5_left_back_thigh_index] = s5_left_back_thigh_close;
    ptr[s6_left_back_leg_index] = s6_left_back_leg_center;
    ptr[s7_left_front_thigh_index] = s7_left_front_thigh_center;
    ptr[s8_left_front_leg_index] = s8_left_front_leg_dn;
    runServo(ptr, total_joins, delay);

    int turn_left_b[total_joins];
    ptr = turn_left_b;
    ptr[s1_right_front_thigh_index] = s1_right_front_thigh_close;
    ptr[s2_right_front_leg_index] = s2_right_front_leg_dn;
    ptr[s3_right_back_thigh_index] = s3_right_back_thigh_center;
    ptr[s4_right_back_leg_index] = s4_right_back_leg_dn;
    ptr[s5_left_back_thigh_index] = s5_left_back_thigh_close;
    ptr[s6_left_back_leg_index] = s6_left_back_leg_dn;
    ptr[s7_left_front_thigh_index] = s7_left_front_thigh_center;
    ptr[s8_left_front_leg_index] = s8_left_front_leg_dn;
    runServo(ptr, total_joins, delay);

    int turn_left_c[total_joins];
    ptr = turn_left_c;
    ptr[s1_right_front_thigh_index] = s1_right_front_thigh_center;
    ptr[s2_right_front_leg_index] = s2_right_front_leg_dn;
    ptr[s3_right_back_thigh_index] = s3_right_back_thigh_close;
    ptr[s4_right_back_leg_index] = s4_right_back_leg_center;
    ptr[s5_left_back_thigh_index] = s5_left_back_thigh_center;
    ptr[s6_left_back_leg_index] = s6_left_back_leg_dn;
    ptr[s7_left_front_thigh_index] = s7_left_front_thigh_close;
    ptr[s8_left_front_leg_index] = s8_left_front_leg_center;
    runServo(ptr, total_joins, delay);

    int turn_left_d[total_joins];
    ptr = turn_left_d;
    ptr[s1_right_front_thigh_index] = s1_right_front_thigh_center;
    ptr[s2_right_front_leg_index] = s2_right_front_leg_dn;
    ptr[s3_right_back_thigh_index] = s3_right_back_thigh_close;
    ptr[s4_right_back_leg_index] = s4_right_back_leg_dn;
    ptr[s5_left_back_thigh_index] = s5_left_back_thigh_center;
    ptr[s6_left_back_leg_index] = s6_left_back_leg_dn;
    ptr[s7_left_front_thigh_index] = s7_left_front_thigh_close;
    ptr[s8_left_front_leg_index] = s8_left_front_leg_dn;
    runServo(ptr, total_joins, delay);
}

void TurtlebotKine::_7_dance() {
    int left_turn_a[total_joins];
    ptr = left_turn_a;
    ptr[s1_right_front_thigh_index] = s1_right_front_thigh_close;
    ptr[s2_right_front_leg_index] = s2_right_front_leg_dn;
    ptr[s3_right_back_thigh_index] = s3_right_back_thigh_close;
    ptr[s4_right_back_leg_index] = s4_right_back_leg_up;
    ptr[s5_left_back_thigh_index] = s5_left_back_thigh_close;
    ptr[s6_left_back_leg_index] = s6_left_back_leg_dn;
    ptr[s7_left_front_thigh_index] = s7_left_front_thigh_close;
    ptr[s8_left_front_leg_index] = s8_left_front_leg_up;
    runServo(ptr, total_joins, delay);

    int left_turn_b[total_joins];
    ptr = left_turn_b;
    ptr[s1_right_front_thigh_index] = s1_right_front_thigh_open;
    ptr[s2_right_front_leg_index] = s2_right_front_leg_up;
    ptr[s3_right_back_thigh_index] = s3_right_back_thigh_open;
    ptr[s4_right_back_leg_index] = s4_right_back_leg_dn;
    ptr[s5_left_back_thigh_index] = s5_left_back_thigh_open;
    ptr[s6_left_back_leg_index] = s6_left_back_leg_up;
    ptr[s7_left_front_thigh_index] = s7_left_front_thigh_open;
    ptr[s8_left_front_leg_index] = s8_left_front_leg_dn;
    runServo(ptr, total_joins, delay);
}

void TurtlebotKine::_4_turnRight() {
    int turn_right_a[total_joins];
    ptr = turn_right_a;
    ptr[s1_right_front_thigh_index] = s1_right_front_thigh_center;
    ptr[s2_right_front_leg_index] = s2_right_front_leg_dn;
    ptr[s3_right_back_thigh_index] = s3_right_back_thigh_open;
    ptr[s4_right_back_leg_index] = s4_right_back_leg_center;
    ptr[s5_left_back_thigh_index] = s5_left_back_thigh_center;
    ptr[s6_left_back_leg_index] = s6_left_back_leg_dn;
    ptr[s7_left_front_thigh_index] = s7_left_front_thigh_open;
    ptr[s8_left_front_leg_index] = s8_left_front_leg_center;
    runServo(ptr, total_joins, delay);

    int turn_right_b[total_joins];
    ptr = turn_right_b;
    ptr[s1_right_front_thigh_index] = s1_right_front_thigh_center;
    ptr[s2_right_front_leg_index] = s2_right_front_leg_dn;
    ptr[s3_right_back_thigh_index] = s3_right_back_thigh_open;
    ptr[s4_right_back_leg_index] = s4_right_back_leg_dn;
    ptr[s5_left_back_thigh_index] = s5_left_back_thigh_center;
    ptr[s6_left_back_leg_index] = s6_left_back_leg_dn;
    ptr[s7_left_front_thigh_index] = s7_left_front_thigh_open;
    ptr[s8_left_front_leg_index] = s8_left_front_leg_dn;
    runServo(ptr, total_joins, delay);

    int turn_right_c[total_joins];
    ptr = turn_right_c;
    ptr[s1_right_front_thigh_index] = s1_right_front_thigh_open;
    ptr[s2_right_front_leg_index] = s2_right_front_leg_center;
    ptr[s3_right_back_thigh_index] = s3_right_back_thigh_center;
    ptr[s4_right_back_leg_index] = s4_right_back_leg_dn;
    ptr[s5_left_back_thigh_index] = s5_left_back_thigh_open;
    ptr[s6_left_back_leg_index] = s6_left_back_leg_center;
    ptr[s7_left_front_thigh_index] = s7_left_front_thigh_center;
    ptr[s8_left_front_leg_index] = s8_left_front_leg_dn;
    runServo(ptr, total_joins, delay);

    int turn_right_d[total_joins];
    ptr = turn_right_d;
    ptr[s1_right_front_thigh_index] = s1_right_front_thigh_open;
    ptr[s2_right_front_leg_index] = s2_right_front_leg_dn;
    ptr[s3_right_back_thigh_index] = s3_right_back_thigh_center;
    ptr[s4_right_back_leg_index] = s4_right_back_leg_dn;
    ptr[s5_left_back_thigh_index] = s5_left_back_thigh_open;
    ptr[s6_left_back_leg_index] = s6_left_back_leg_dn;
    ptr[s7_left_front_thigh_index] = s7_left_front_thigh_center;
    ptr[s8_left_front_leg_index] = s8_left_front_leg_dn;
    runServo(ptr, total_joins, delay);

}

